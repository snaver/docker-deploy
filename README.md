# Docker Deploy #

Custom Docker image for deployment of a Laravel 5.5 stack.

## Building Docker Image ##

Taken from https://community.atlassian.com/t5/Bitbucket-questions/How-do-I-create-a-docker-image-for-Bitbucket-Pipelines/qaq-p/334724#M11732.

- Run `docker build -t snaver/docker-deploy .` to build the image
- Run `docker push snaver/docker-deploy` to push changes to Docker Hub

## Testing Docker Image ##

If you'd like to test the image locally..

- Run `docker run -i -t -v ${PWD}:/var/www snaver/docker-deploy /bin/bash`
