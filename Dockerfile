# Docker Deploy
#
# VERSION               0.0.1

# start with the official Composer image and name it
FROM composer:1.5.1 AS composer

LABEL Description="Custom Docker image for deployment of a Laravel 5.5 stack." Vendor="Richard Evans" Version="0.0.1"
MAINTAINER Richard Evans <github@snaver.net>

FROM php:7.1-cli

RUN apt-get update && \
    apt-get install -y --no-install-recommends git zip unzip ssh rsync && \
    apt-get -y autoremove && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

RUN docker-php-ext-install mysqli

# copy the Composer PHAR from the Composer image into the PHP image
COPY --from=composer /usr/bin/composer /usr/bin/composer

RUN composer global require laravel/envoy

ENV PATH="/root/.composer/vendor/bin:${PATH}"
